package org.tw;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SalesTest {

    @Test
    void should_not_return_any_change_when_person_paid_exact_amount_for_three_brownies_two_muffins_one_cake_pop() {
        Sales personOne = new Sales(new String[]{"B", "M", "C"}, new int[]{3, 2, 1}, 530);
        Sales change = new Sales(0);

        assertThat(personOne.calculatingCostOfSale(), is(equalTo(change)));
    }

    @Test
    void should_not_return_any_change_when_person_paid_exact_amount_for_one_cake_pop_and_one_water_bottle() {
        Sales personOne = new Sales(new String[]{"C", "W"}, new int[]{1, 1}, 285);
        Sales change = new Sales(0);

        assertThat(personOne.calculatingCostOfSale(), is(equalTo(change)));
    }

    @Test
    void should_return_change_when_person_overpay() {
        Sales Sony = new Sales(new String[]{"B", "M"}, new int[]{3, 2}, 400);
        Sales change = new Sales(5);

        assertThat(Sony.calculatingCostOfSale(), is(equalTo(change)));
    }

    @Test
    void should_return_not_enough_money_when_person_has_not_enough_money_to_buy_items() {
        Sales personThree = new Sales(new String[]{"C", "M"}, new int[]{2, 1}, 40);
        Sales noEnoughMoney = new Sales("Not enough money");

        assertThat(personThree.calculatingCostOfSale(), is(equalTo(noEnoughMoney)));
    }

    @Test
    void should_return_not_enough_stock_when_given_quantity_more_than_stock_quantity() {
        Sales personFour = new Sales(new String[]{"B", "C"}, new int[]{3, 27}, 899);
        Sales notEnoughStock = new Sales("Not enough stock");

        assertThat(personFour.calculatingCostOfSale(), is(equalTo(notEnoughStock)));
    }
}
