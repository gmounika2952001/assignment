package org.tw;

public class Sales {

    int costOfBrownie = 65, costOfMuffin = 100, costOfCakePop = 135, costOfWater = 150;
    int quantityOfBrownie = 30, quantityOfMuffin = 36, quantityOfCakePop = 24, quantityOfWater = 30;
    String[] listOfItems;
    int[] quantityOfItems;
    int amountPaid, totalCost = 0;
    int change;
    String messageToCustomer;

    Sales(String[] listOfItems, int[] quantityOfItems, int amountPersonHas) {
        this.listOfItems = listOfItems;
        this.quantityOfItems = quantityOfItems;
        this.amountPaid = amountPersonHas;
    }

    Sales(int change) {
        this.change = change;
    }

    Sales(String messageToCustomer) {
        this.messageToCustomer = messageToCustomer;
    }

    public Sales calculatingCostOfSale() {
        for (int i = 0; i < listOfItems.length; i++) {

            switch (listOfItems[i]) {
                case "B":
                    if (quantityOfItems[i] <= quantityOfBrownie) {
                        totalCost += costOfBrownie * quantityOfItems[i];
                        quantityOfBrownie -= quantityOfItems[i];
                    } else
                        return new Sales("Not enough stock");

                    break;
                case "M":
                    if (quantityOfItems[i] <= quantityOfMuffin) {
                        totalCost += costOfMuffin * quantityOfItems[i];
                        quantityOfMuffin -= quantityOfItems[i];
                    } else
                        return new Sales("Not enough stock");

                    break;
                case "W":
                    if (quantityOfItems[i] <= quantityOfWater) {
                        totalCost += costOfWater * quantityOfItems[i];
                        quantityOfWater -= quantityOfItems[i];
                    } else
                        return new Sales("Not enough stock");
                    break;
                case "C":
                    if (quantityOfItems[i] <= quantityOfCakePop) {
                        totalCost += costOfCakePop * quantityOfItems[i];
                        quantityOfCakePop -= quantityOfItems[i];
                    } else
                        return new Sales("Not enough stock");
                    break;
            }
        }
        if (amountPaid < totalCost)
            return new Sales("Not enough money");
        return new Sales(amountPaid - totalCost);
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Sales that = (Sales) o;
        return ((that.change == this.change) || (this.messageToCustomer.equals(that.messageToCustomer)));
    }
}
